module Utilities where

import Data.List(foldl', scanl', group)
import GHC.Exts(sortWith)
import Data.Bits(testBit)
import System.Random.Shuffle(shuffleM)
import System.Environment(getArgs)

froto:: [a]->[(a,a)]
froto [] = []
froto (first:[]) = []
froto (first:second:rest) =  (first,second) : froto (second:rest)

frotoByNearness:: Ord a => Num a => [a] -> [(a,a)]
frotoByNearness x = sortWith(\(a,b) -> abs(a - b)) (froto x)

nearestPair :: Num a => Ord a => [a] -> (a,a)
nearestPair = head . frotoByNearness

mapButLast :: (a -> a) ->[a]->[a]
mapButLast transf list = (map (transf) (init list)) ++ [last list]

implode :: (Show a) => String -> [a] -> String
implode sym = concat . mapButLast (++ sym) . map show

normspace = concat . map xs .group
 where
   xs g
      |head g == ' ' = " "
      |otherwise = g

data Direction = North|South|East|West deriving (Show)

move1 cor North = (fst cor, snd cor +1)
move1 cor South = (fst cor, snd cor -1)
move1 cor East = (fst cor +1, snd cor)
move1 cor West = (fst cor - 1, snd cor)

moveN :: Num a => (a,a) -> [Direction] -> (a,a)
moveN = foldl' move1

intermoves :: Num a => (a,a) -> [Direction] -> [(a,a)]
intermoves = scanl' move1
