module MindReader where

import Data.List(foldl', scanl', group)
import GHC.Exts(sortWith)
import Data.Bits(testBit)
import System.Random.Shuffle(shuffleM)
import System.Environment(getArgs)

intsWithBit :: Int -> Int -> [Int]
intsWithBit max base = filter(\a-> testBit a base == True) [1..max]

binaryToDecimal :: String -> Int
binaryToDecimal = foldl'(\acc x -> acc*2 + if x == '1' then 1 else 0) 0

decimalToBinary' :: Int -> String
decimalToBinary' x 
   |x == 0 = "" 
   |x `mod` 2== 0 = "0" ++ decimalToBinary'(x`div`2) 
   |x `mod` 2 == 1 = "1" ++ decimalToBinary'(x`div`2)

decimalToBinary :: Int -> String
decimalToBinary x = reverse(if x == 0 then "0" else decimalToBinary' x)

nbits :: Int -> Int
nbits = length . decimalToBinary

promptForBit :: Int -> Int -> IO Int
promptForBit max index = do 
   putStrLn ""
   print $ intsWithBit max index
   putStr "Is your number in this list? y/[n]? "
   line <- getLine
   if line == "y" then return 1 else return 0

main = do
   putStr "What's your favorite upper bound? "
   input <- getLine
   putStrLn ""
   putStrLn $ "Think of a number in [1, " ++ input ++"]. But don't tell me. I will read your mind!" 
   let max = read input :: Int
   let bits = nbits(max)
   let arr = [0..bits-1]
   let result = mapM((\a-> promptForBit max a)) arr
   blah <- result
   let guess = binaryToDecimal(reverse (concat(map show blah)))
   putStrLn ""
   putStrLn $ "Your number is " ++ (show guess) ++ "."
   return ()
