module BullsAndCows where

import Data.List(foldl', scanl', group)
import GHC.Exts(sortWith)
import Data.Bits(testBit)
import System.Random.Shuffle(shuffleM)
import System.Environment(getArgs)

bulls :: String -> String -> String
bulls target guess = map fst (filter(\(a,b) -> a == b) (zip target guess))

nonbulls :: String -> String -> (String, String)
nonbulls target guess = unzip (filter(\(a,b) -> a/=b) (zip target guess))

cows :: String -> String -> String
cows target guess = filter(\a -> elem a(fst(nonbulls target guess))) (snd(nonbulls target guess))

score :: String -> String -> (Int, Int)
score target guess = (length (bulls target guess), length(cows target guess))

game :: String -> IO()
game target = do
   putStr "Guess: "
   guess <- getLine
   let arr = score target guess
   putStrLn $ "Bulls: " ++ show(fst arr) ++ " | Cows: " ++ show(snd arr)
   if fst arr == length(target) then return () else game target

randomTarget :: Int -> IO String
randomTarget l = do 
  shuf <- shuffleM ['0'..'9']
  return (take l (shuf))

main = do
   l <- getArgs
   let stri  = head l
   let num = read stri :: Int
   random <- randomTarget num
   game random
