module Intervals where

import Data.List(foldl', scanl', group)
import GHC.Exts(sortWith)
import Data.Bits(testBit)
import System.Random.Shuffle(shuffleM)
import System.Environment(getArgs)

within :: Integer -> (Integer, Integer) -> Bool
within i (a,b) = i >= a && i <=b

withins :: Integer -> [(Integer, Integer)] -> [(Integer, Integer)]
withins i cor = filter (within i) cor 

isFree :: Integer -> [(Integer, Integer)] -> Bool
isFree i cor = withins i cor == []

freeBeyond :: [(Integer, Integer)] -> Integer
freeBeyond list = snd(last(sortWith (\(a,b)-> b) list)) + 1

firstFree :: Integer -> [(Integer, Integer)] -> Integer
firstFree i list
   |isFree i list == True = i
   |otherwise = firstFree (i+1) list
