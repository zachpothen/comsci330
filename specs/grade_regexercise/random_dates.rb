#!/usr/bin/env ruby

require 'forgery'
generator = Random.new

50.times do
  case generator.rand 100
    when 0..25
      print Forgery(:lorem_ipsum).words(30)
    when 26..79
      date = Forgery(:date).date
      print date.strftime(' %-m/%-d/%Y ')
    when 80..99
      puts
  end
end
