#!/usr/bin/env zsh

# ---------------------------------------------------------------------------- 

# Assert that each file has the required tokens.
function check_tokens() {
  file=$1
  shift
  for i in $*; do
    grep -- $i $file >/dev/null
    if [[ $? -ne 0 ]]; then
      echo "Yikes! Expected $i in $file not found." >& 2
      exit 1
    fi
  done
}

# ---------------------------------------------------------------------------- 

iskey=0
isgrader=0
onlylater=0
args=""
prefix=""
while getopts kgl opt; do
  case $opt in
    (k)
      iskey=1
      args="$args -k"
      ;;
    (g)
      prefix="forn 30"
      isgrader=1
      args="$args -g"
      ;;
    (l)
      onlylater=1
      ;;
  esac
done

graderdir=${0:A:h}
srcdir=$(pwd)
libdir=$srcdir/../.tmp
basedir=${srcdir:t}
tmp="${srcdir}/.tmp"
name=regexercise
course=cs330
semester=2018a
actual_version=1

# Assert version
read expected_version < =(curl -s "https://twodee.org/teaching/vspec.php?course=$course&semester=$semester&homework=$name")
if [[ $expected_version != $actual_version ]]; then
  echo "Your SpecChecker appears to be out of date. Follow the directions "
  echo "in homework 0, part 3 to synchronize it."
  echo
  exit 1
fi

mkdir -p $libdir/to_trash
rm -rf $libdir/to_trash/*

# Assert that we're in the right directory.
if [[ $basedir != $name ]]; then
  echo "Oh no! The grader must be run from the $name directory." >&2
  exit 1
fi

# ---------------------------------------------------------------------------- 

echo "-----------------------------------------------------"
echo "Running tests to qualify for later-week submission..."
echo
${=prefix} ruby -I$graderdir $graderdir/later_tests.rb ${=args}
if [[ $? -ne 0 ]]; then
  echo "-----------------------------------------------------"
  echo
  exit 1
fi

echo "-----------------------------------------------------"
echo "Running tests to qualify for full credit..."
echo
${=prefix} ruby -I$graderdir $graderdir/full_tests.rb ${=args}
if [[ $? -ne 0 ]]; then
  echo "-----------------------------------------------------"
  echo
  exit 2
fi
echo "-----------------------------------------------------"
echo

# ---------------------------------------------------------------------------- 

# Assert that a commit and push has happened or is about to happen.
if [[ $isgrader -eq 0 ]]; then
  echo "Have you added any unadded files, committed, and pushed to Bitbucket?"
  echo -n "Run \"git status\" if you don't know. y or [n]? "
  read answer
  echo
  if [[ "$answer" != "y" ]]; then
    exit 2
  fi
fi

exit 0
