import Utilities
import System.Exit

-- --------------------------------------------------------------------------- 

status = 1

assertGeneralEquals message isEqual expected actual = do
  if isEqual expected actual then
    return ()
  else do
    putStrLn message
    putStr $ "  Expected: "
    print expected
    putStr $ "    Actual: "
    print actual
    System.Exit.exitWith (ExitFailure status)
    return ()

assertEquals message expected actual = 
  assertGeneralEquals message (==) expected actual

assertTrue message actual = assertEquals message True actual
assertFalse message actual = assertEquals message False actual

-- --------------------------------------------------------------------------- 

assertNormspace s expected =
  assertEquals ("normspace \"" ++ s ++ "\" gives an unexpected value.") expected $ normspace s

assertFroto l expected =
  assertEquals ("froto " ++ (show l) ++ " gives an unexpected value.") expected $ froto l

assertFrotoByNearness l expected =
  assertEquals ("frotoByNearness " ++ (show l) ++ " gives an unexpected value.") expected $ frotoByNearness l

assertNearestPair l expected =
  assertEquals ("nearestPair " ++ (show l) ++ " gives an unexpected value.") expected $ nearestPair l

assertMapButLast fdescriptor f l expected =
  assertEquals ("mapButLast " ++ fdescriptor ++ " " ++ (show l) ++ " gives an unexpected value.") expected $ mapButLast f l

assertImplode separator l expected =
  assertEquals ("implode \"" ++ separator ++ "\" " ++ (show l) ++ " gives an unexpected value.") expected $ implode separator l

assertMove1 p dir expected =
  assertEquals ("move1 " ++ (show p) ++ " " ++ (show dir) ++ " " ++ " gives an unexpected value.") expected $ move1 p dir

assertMoveN p dirs expected =
  assertEquals ("moveN " ++ (show p) ++ " " ++ (show dirs) ++ " " ++ " gives an unexpected value.") expected $ moveN p dirs

assertIntermoves p dirs expected =
  assertEquals ("intermoves " ++ (show p) ++ " " ++ (show dirs) ++ " " ++ " gives an unexpected value.") expected $ intermoves p dirs

-- --------------------------------------------------------------------------- 

newtype PlainChar = PlainChar Char
instance Show PlainChar where
  show (PlainChar c) = c : ""
stringToPlainChars s = map PlainChar s

main = do
  -- froto
  assertFroto [1..5] [(1,2), (2,3), (3,4), (4,5)]
  assertFroto "wxyz" [('w','x'), ('x', 'y'), ('y', 'z')]
  assertFroto [1, 90, 43, -50, 7] [(1, 90), (90, 43), (43, -50), (-50, 7)]
  assertFroto [35, 89, 30, 90, -49, 24, -66, 99, 68, 52, 6, 97, 69, 29, 27, 43, -93, -17, -10, 65, 58, 77, 69, -57, 48, -16, 63, -86, -12, -80, 27, -42, -80, 55, 98, 7, 15, -10, 34, -28, -14, -94, 7, -25, 63, 7, -15, 63, 77, -34, -72, -31, -78, 68, 12, 26, -65, -32, 4, -36, -19, -2, -46, -69, 28, -28] [(35,89),(89,30),(30,90),(90,-49),(-49,24),(24,-66),(-66,99),(99,68),(68,52),(52,6),(6,97),(97,69),(69,29),(29,27),(27,43),(43,-93),(-93,-17),(-17,-10),(-10,65),(65,58),(58,77),(77,69),(69,-57),(-57,48),(48,-16),(-16,63),(63,-86),(-86,-12),(-12,-80),(-80,27),(27,-42),(-42,-80),(-80,55),(55,98),(98,7),(7,15),(15,-10),(-10,34),(34,-28),(-28,-14),(-14,-94),(-94,7),(7,-25),(-25,63),(63,7),(7,-15),(-15,63),(63,77),(77,-34),(-34,-72),(-72,-31),(-31,-78),(-78,68),(68,12),(12,26),(26,-65),(-65,-32),(-32,4),(4,-36),(-36,-19),(-19,-2),(-2,-46),(-46,-69),(-69,28),(28,-28)]
  assertFroto [13, -31, -15, 1, -3, -58, 25, 94, 92, -44, 49, 14, -44, -73, -33, 97, -13, 15] [(13,-31),(-31,-15),(-15,1),(1,-3),(-3,-58),(-58,25),(25,94),(94,92),(92,-44),(-44,49),(49,14),(14,-44),(-44,-73),(-73,-33),(-33,97),(97,-13),(-13,15)] 
  assertFroto ([] :: [Integer]) []

  -- frotoByNearness
  assertFrotoByNearness [-65, 2, -88, 43, -70, 20, 89, 71] [(89,71),(-65,2),(20,89),(2,-88),(-70,20),(43,-70),(-88,43)]
  assertFrotoByNearness [61, 23, -73, -42, 8, -81, 44, -55] [(-73,-42),(61,23),(-42,8),(8,-81),(23,-73),(44,-55),(-81,44)]
  assertFrotoByNearness [61, 31, -63, 59, -21, 22, 46, 5, -72, -98, -65, -70, -53, 78, 10, -44, 42, 32, 80, -28, -15, -70, 68, -32, -35, -21] [(-32,-35),(-65,-70),(42,32),(-28,-15),(-35,-21),(-70,-53),(22,46),(-72,-98),(61,31),(-98,-65),(46,5),(-21,22),(32,80),(10,-44),(-15,-70),(78,10),(5,-72),(59,-21),(-44,42),(31,-63),(68,-32),(80,-28),(-63,59),(-53,78),(-70,68)]
  assertFrotoByNearness [21, -69, -94, 97, -47, -53, 3, 99, -62, 62, 45, 80, -53, -70, 4, -92, 73, 95, -98, -50, 71, -48, -81, -95, -71, -19, 11, -92, -57, 99, 93, -77, -39, 79, -98, 90, 2, -60] [(-47,-53),(99,93),(-81,-95),(62,45),(-53,-70),(73,95),(-95,-71),(-69,-94),(-19,11),(-48,-81),(45,80),(-92,-57),(-77,-39),(-98,-50),(-71,-19),(-53,3),(2,-60),(-70,4),(90,2),(21,-69),(3,99),(4,-92),(11,-92),(-39,79),(71,-48),(-50,71),(-62,62),(80,-53),(97,-47),(-57,99),(99,-62),(-92,73),(93,-77),(79,-98),(-98,90),(-94,97),(95,-98)]
  assertFrotoByNearness [] []

  -- nearestPair
  assertNearestPair [-14, -59, -64] (-59,-64)
  assertNearestPair [49, 32, -73, 98, 49, -98, -83, -72, -77, -57] (-72,-77)
  assertNearestPair [-64, 14, 62, -96, -48, 11, -88, 90, -68, -66, 97, 11, 9, 52, -84, 82, 79, -29, 97, -11] (-68,-66)
  assertNearestPair [81, 33, 1, 83, -52, 41, 66, -21, -16, -70, 87, -36, -24, 90, -7, 30] (-21,-16)

  -- mapButLast
  assertMapButLast "double" (* 2) [1..4] [2, 4, 6, 4]
  assertMapButLast "add3" (+ 3) [100,99..95] [103, 102, 101, 100, 99, 95]
  assertMapButLast "negate" negate [(-5)..5] ([5,4..(-4)] ++ [5])
  assertMapButLast "half" (`quot` 2) [10, 11, 20, -5, 3, 17, 18, 19, 20] [5, 5, 10, -2, 1, 8, 9, 9, 20]

  -- implode
  assertImplode "," [1..5] "1,2,3,4,5"
  assertImplode "-" (stringToPlainChars "werewe") "w-e-r-e-w-e"
  assertImplode "\t" [9, 5, 1, 100] "9\t5\t1\t100"
  assertImplode " | " [(1,19), (7,3), (8, -4), (10, 100)] "(1,19) | (7,3) | (8,-4) | (10,100)"

  -- normspace
  assertNormspace "a  b c" "a b c"
  assertNormspace "   the       quick                 brown | fox,jumps   oooooooooooover the l a z y        brown  dog    " " the quick brown | fox,jumps oooooooooooover the l a z y brown dog "
  assertNormspace "     " " "
  assertNormspace "1  2 3 4       " "1 2 3 4 "

  -- move1
  assertMove1 (10, 13) North (10, 14)
  assertMove1 (10, 13) East (11, 13)
  assertMove1 (10, 13) South (10, 12)
  assertMove1 (10, 13) West (9, 13)
  assertMove1 (95, 1007) North (95, 1008)
  assertMove1 (95, 1007) East (96, 1007)
  assertMove1 (95, 1007) South (95, 1006)
  assertMove1 (95, 1007) West (94, 1007)

  -- moveN
  assertMoveN (60, 82) [West, West, East, West, North, West, West, North, East, West, East, East, North, South, North, North, South, West, South, South, South, West, West, East, North, North, South, East, East, West, West] (56,83)
  assertMoveN (148, 19) [South, West, South, West, West, East, North, North, West, South, South, North, East, West, South, South, South, West, East, West, North, South, North, West, West, East, North, North, North, South, North] (143,19)
  assertMoveN (120, 152) [North, West, North, South, West, South, East, North, West, East, South, East, West, North, South, East, South, South, South, North, East, West, North, West, West, East, North, West, West, South, East, North, South, East, North, East, West, North, North, South, East, East, North, East, West, West, South, East, North] (121,154)
  assertMoveN (178, 79) [East, North, North, South, West, West, South, East, East, East, West, East, North, East, East, North, East, East, South, South, South, West, East, East, East, North, South, South, East, West, South] (186,76)
  assertMoveN (127, 61) [South, West] (126,60)
  assertMoveN (84, 5) [] (84,5)

  -- intermoves
  assertIntermoves (130, 188) [East, North, South, North, South, North, North, South, North, South, North, East, North, East, South, North, South, East, East, South, East] [(130,188),(131,188),(131,189),(131,188),(131,189),(131,188),(131,189),(131,190),(131,189),(131,190),(131,189),(131,190),(132,190),(132,191),(133,191),(133,190),(133,191),(133,190),(134,190),(135,190),(135,189),(136,189)]
  assertIntermoves (198, 71) [South, South, East, North, North, West, West, East, West, North, West, South, West, North, North, West, North, South] [(198,71),(198,70),(198,69),(199,69),(199,70),(199,71),(198,71),(197,71),(198,71),(197,71),(197,72),(196,72),(196,71),(195,71),(195,72),(195,73),(194,73),(194,74),(194,73)]
  assertIntermoves (92, 125) [West, South, West, East, East, North, North, West, West, East, West, East, South, North, North, South, South, South, South, East, North, East, South, East, West, West, South, East, North, East, West, West, North, West, East, East, West] [(92,125),(91,125),(91,124),(90,124),(91,124),(92,124),(92,125),(92,126),(91,126),(90,126),(91,126),(90,126),(91,126),(91,125),(91,126),(91,127),(91,126),(91,125),(91,124),(91,123),(92,123),(92,124),(93,124),(93,123),(94,123),(93,123),(92,123),(92,122),(93,122),(93,123),(94,123),(93,123),(92,123),(92,124),(91,124),(92,124),(93,124),(92,124)]
  assertIntermoves (177, 97) [North, West, West, West, South, East, East, South, North, West, East, South, West] [(177,97),(177,98),(176,98),(175,98),(174,98),(174,97),(175,97),(176,97),(176,96),(176,97),(175,97),(176,97),(176,96),(175,96)]
  assertIntermoves (50, 67) [] [(50,67)]
