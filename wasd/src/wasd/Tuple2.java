package wasd;

import java.util.Scanner;

public class Tuple2<T, U> {

    private T fVal;
    private U sVal;


    public Tuple2(T fVal, U sVal) {
        this.fVal = fVal;
        this.sVal = sVal;
    }

    public T getFirst() { return this.fVal; }

    public U getSecond() {
        return this.sVal;
    }

}