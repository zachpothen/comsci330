package wasd;

public class Player {
    private Vector2 start, look;
    private double fov;
    private World world;

    public Player(World w, double fov) {
        start = w.getStart();
        look = w.getLookAt();
        world = w;
        this.fov = fov;
    }

    public double getFieldOfView() {
        return fov;
    }

    public Vector2 getPosition() {
        return start;
    }

    public Vector2 getLookAt() {
        return look;
    }

    public Vector2 getRight() {
        return look.rotate(-90);
    }

    public void rotate(double degree) {
        look = look.rotate(degree);
    }

    public void advance(double distance) {
        Vector2 loc = look.normalize().multiply(distance);

        if (world.isPassage(start.add(loc))) {
            start = start.add(loc);
        }
    }

    public void strafe(double distance) {
        Vector2 lookRight = getRight();
        Vector2 loc = lookRight.normalize().multiply(distance);

        if (world.isPassage(start.add(loc))) {
            start = start.add(loc);
        }
    }

}
