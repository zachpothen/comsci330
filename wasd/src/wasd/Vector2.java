package wasd;

public class Vector2 {

    private double x;
    private double y;

    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double length() {
        return Math.sqrt(x*x + y*y);
    }

    public Vector2 add(Vector2 v) {
        return new Vector2(x + v.getX(), y + v.getY());
    }

    public Vector2 subtract(Vector2 v) {
        return new Vector2(x - v.getX(), y - v.getY());
    }

    public Vector2 multiply(double scaleFactor) {
        return new Vector2(x * scaleFactor, y * scaleFactor);
    }

    public Vector2 divide(double scaleFactor) {
        return new Vector2(x / scaleFactor, y / scaleFactor);
    }

    public Vector2 normalize() {
        return new Vector2(x* 1/length(), y * 1/length());
    }

    public Vector2 rotate(double degrees) {
        double rotatedX = x*Math.cos(Math.toRadians(degrees)) - y* Math.sin(Math.toRadians(degrees));
        double rotatedY = x*Math.sin(Math.toRadians(degrees)) + y*Math.cos(Math.toRadians(degrees));
        return new Vector2(rotatedX,rotatedY);
    }

    public String toString() {
        return String.format("%f,%f", x, y);
    }

}