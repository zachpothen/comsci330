package wasd;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Raycaster {


    private ThreadPool tPool;
    private ReentrantLock lock;
    private World world;
    private Player player;

    public Raycaster(World w, Player p, int threadNum) {
        world = w;
        player = p;
       tPool = new ThreadPool(threadNum);
       lock = new ReentrantLock();

    }

    private void drawColumn(int width, int height, Graphics drawingSurface, int colIndex){

        //Step 1
        double maxColumn = width -1;
        double horizontalProportion = ((double) colIndex)/maxColumn;

        //Step 2
        //Positive = right; Negative = left; 0 = straight
        double fov = (horizontalProportion*2.0)-1.0;

        //Step 3
        double angleOfRay = ((player.getFieldOfView()/2.0) * fov);

        //Step 4
        angleOfRay *= -1.0;

        //Step 5
        Vector2 ray =  player.getLookAt().rotate(angleOfRay);

        //Step 6
        Tuple2 zero = world.getDistanceToWall(player.getPosition(), ray.normalize());
        double nearestWall  = (double) zero.getFirst();

        //Step 7
        double pixelHeight = ((double) height)/nearestWall;

        //Step 8
        if((boolean) zero.getSecond()) {
            drawingSurface.setColor(new Color(52, 156, 60));
        }else{
            drawingSurface.setColor(new Color(25, 71, 25));
        }
        drawingSurface.drawLine(colIndex, (int)((height/2)-(pixelHeight/2)), colIndex, (int)((height/2)+(pixelHeight/2)));

    }

    public BufferedImage render(int width, int height) {

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics graphics = image.getGraphics();
        graphics.setColor(new Color(0,0,0));
        graphics.fillRect(0, 0, width, height);



        final Condition done = lock.newCondition();
        final AtomicInteger counter = new AtomicInteger(0);
        lock.lock();

            for(int i = 0; i < width; i++) {
                final int c = i;
                tPool.schedule(() -> {
                    drawColumn(width, height, graphics, c);
                    lock.lock();
                    if (counter.incrementAndGet() == width) {
                        done.signal();
                    }
                    lock.unlock();
                });

            }

            done.awaitUninterruptibly();
            lock.unlock();

        return image;
    }

    public void stop() {
        tPool.stop();
    }

}
