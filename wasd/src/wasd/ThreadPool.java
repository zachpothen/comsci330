package wasd;

import java.util.concurrent.LinkedBlockingQueue;


public class ThreadPool{
	
    private LinkedBlockingQueue<BooleanTask> queue;
    private int numThreads;
    private Thread[] threads;

    public ThreadPool(int numThreads) {

        this.numThreads = numThreads;
        queue = new LinkedBlockingQueue<BooleanTask>();
        threads = new Thread[numThreads];

        for(int i = 0; i < numThreads; i++){

            threads[i] = new Thread(() -> {
                boolean poisoned = false;

                while (!poisoned) {
                    try {
                        poisoned = queue.take().run();
                    } catch (InterruptedException e) { }
                }
            });

            threads[i].start();
        }
        for (Thread t:threads) {
            System.out.println("threads" + t.getId());
        }
    }

    public void schedule(Runnable r){
        try {
            queue.put(() ->{
                    r.run();
                    return false;
            });
        } catch (InterruptedException e) {
        }
    }

    public void stop() {

        for (int i = 0; i < numThreads; i++) {
            boolean scheduled = false;
            while (!scheduled) {
                try {
                    queue.put(() -> {
                            return true;
                    });
                    scheduled = true;
                } catch (InterruptedException e) {

                }
            }
        }

        for(int i = 0; i < numThreads; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
            }
        }
    }

}