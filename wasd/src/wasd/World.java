package wasd;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class World {

    private int width, height;
    private  double playerX, playerZ , lookAtX, lookAtZ;
    private static int[][] map;

    public World(File f) throws IOException {

        Scanner input = new Scanner(f);

        width = input.nextInt();
        height = input.nextInt();

        playerX = input.nextDouble();
        playerZ = input.nextDouble();

        lookAtX = input.nextDouble();
        lookAtZ = input.nextDouble();

        map = new int[width][height];

        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                map[w][h] = input.nextInt();
            }
        }
    }

    public Vector2 getStart() {
        return new Vector2(playerX, playerZ);
    }

    public  Vector2 getLookAt() {
        return new Vector2(lookAtX, lookAtZ);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public  int get(int xCor, int zCor) {

        if(width > xCor && height > zCor && xCor >= 0 && zCor >= 0) {
            return map[xCor][zCor];
        }else{
            return 1;
        }
    }

    public boolean isPassage(int xCor, int zCor) {
        return get(xCor, zCor) == 0;
    }

    public boolean isPassage(Vector2 v) {
        return get((int) v.getX(), (int) v.getY()) == 0;
    }

    public Tuple2 getDistanceToWall(Vector2 local, Vector2 view) {

        int mapX = (int) local.getX();
        int mapY = (int) local.getY();

        double sideDistX;
        double sideDistY;


        double deltaDistX = Math.abs(1 / view.getX());
        double deltaDistY = Math.abs(1 / view.getY());
        double perpWallDist;

        //what direction to step in x or y-direction (either +1 or -1)
        int stepX;
        int stepY;

        Boolean hit = false; //was there a wall hit?
        int side = 0; //was a NS or a EW wall hit?

        //calculate step and initial sideDist
        if (view.getX() < 0) {
            stepX = -1;
            sideDistX = (local.getX() - mapX) * deltaDistX;
        } else {
            stepX = 1;
            sideDistX = (mapX + 1.0 - local.getX()) * deltaDistX;
        }

        if (view.getY() < 0) {
            stepY = -1;
            sideDistY = (local.getY() - mapY) * deltaDistY;
        } else {
            stepY = 1;
            sideDistY = (mapY + 1.0 - local.getY()) * deltaDistY;
        }

        //perform DDA
        while (!hit) {
            //jump to next map square, OR in x-direction, OR in y-direction
            if (sideDistX < sideDistY) {
                sideDistX += deltaDistX;
                mapX += stepX;
                side = 0;
            } else {
                sideDistY += deltaDistY;
                mapY += stepY;
                side = 1;
            }
            //Check if ray has hit a wall
            if (get(mapX,mapY) == 1) hit = true;
        }

        //Calculate distance projected on camera direction (Euclidean distance will give fisheye effect!)
        if (side == 0) {
            perpWallDist = (mapX - local.getX() + (1 - stepX) / 2) / view.getX();
            return new Tuple2(perpWallDist, false);
        } else {
            perpWallDist = (mapY - local.getY() + (1 - stepY) / 2) / view.getY();
            return new Tuple2(perpWallDist, true);
        }
    }
}