#include "Checkbox.h"

	Checkbox::Checkbox(int x, int y, const string s)
		:Widget(x,s.length()+3+x,y,y){//add three to compensate for [X] 
		checked = false;
		CheckboxLabel = s;
	}
	

	bool Checkbox::IsChecked(void) const{
		return checked;
	}

	void Checkbox::IsChecked(bool box){
		checked = box;
	}

	void Checkbox::Draw(){
		//if checked print x
		if(checked){
			mvprintw(rect->GetTop(), rect->GetLeft(), "[x] %s", CheckboxLabel.data());
		}else{
		//else leave blank
			mvprintw(rect->GetTop(), rect->GetLeft(), "[ ] %s", CheckboxLabel.data());
		}
	}

	void Checkbox::AddListener(std::function<void(bool)> listener){
		listeners.push_front(listener);
	}

	void Checkbox::OnMouseClick(int x, int y){
		//set to the opposite of the current state of the checkbox
		if(checked){
			checked = false;
		}else{
			checked = true;
		}
		std::list<std::function<void(bool)>>::iterator iter = listeners.begin();
		while (iter != listeners.end()){
			(*iter)(checked);
			iter++;
		}
	}
