#include "Label.h"
#include "Button.h"
#include "Checkbox.h"
#include "Slider.h"
#include "List.h"
#include "Slider.h"
#include "Window.h"
#include <iostream>
#include <ncurses.h>
#include <string>

	int main(){
		//Create headers for the recommendations and the snacks
		Rectangle r(0,0,1,1);
		Window *w = new Window();
		Label *title = new Label(15,0,"MOVIE RECOMMENDATIONS");
		Label *lb = new Label(0,1,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		Label *desc = new Label(0,2,"Select a genre and a list of reccomendations will appear");
		Label *snack = new Label(15,14, "Snack Shack");
		
		Label *lb2 = new Label(0,15,"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
		//Creating snack checkbox
		int totalPrice = 0;
		Label *tot = new Label(30,17, "Total Price: $" +  std::to_string(totalPrice));
		Checkbox *opt = new Checkbox(5,17,"Popcorn");
		Checkbox *opt2 = new Checkbox(5,18,"Licorice");
		Checkbox *opt3 = new Checkbox(5,19,"Chocolate");
		Checkbox *opt4 = new Checkbox(5,20, "Gummy Worms");
		Checkbox *opt5 = new Checkbox(5,21, "Pop");
		
		//Add listeners to checkbox to add totals
		opt->AddListener([&](bool isPop) {
			
			if(opt->IsChecked()){
				totalPrice += 3;
				tot->SetText("Total Price: $" + std::to_string(totalPrice));
			}else{
				totalPrice += -3;
				tot->SetText("Total Price: $" + std::to_string(totalPrice));
			}
		});
		opt2->AddListener([&](bool isPop) {
			
			if(opt2->IsChecked()){
				totalPrice += 2;
				tot->SetText("Total Price: $" + std::to_string(totalPrice));
			}else{
				totalPrice += -2;
				tot->SetText("Total Price: $" + std::to_string(totalPrice));
			}
		});

		opt3->AddListener([&](bool isPop) {
			
			if(opt3->IsChecked()){
				totalPrice += 2;
				tot->SetText("Total Price: $" + std::to_string(totalPrice));
			}else{
				totalPrice += -2;
				tot->SetText("Total Price: $" + std::to_string(totalPrice));
			}
		});
		

		opt4->AddListener([&](bool isPop) {
			
			if(opt4->IsChecked()){
				totalPrice += 3;
				tot->SetText("Total Price: $" + std::to_string(totalPrice));
			}else{
				totalPrice += -3;
				tot->SetText("Total Price: $" + std::to_string(totalPrice));
			}
		});
		opt5->AddListener([&](bool isPop) {
			
			if(opt5->IsChecked()){
				totalPrice += 1;
				tot->SetText("Total Price: $" + std::to_string(totalPrice));
			}else{
				totalPrice += -1;
				tot->SetText("Total Price: $" + std::to_string(totalPrice));
			}
		});	
		
		//Exit button
		Button *gb = new Button(35, 20, "Enjoy The Movie!!");
		gb->AddListener([&]() {
			w->Stop();
		});
		
		//Button to pull up food menu
		Button *foodOpt = new Button(35, 11, "Start Watching!!");
		foodOpt->AddListener([&]() {
			w->Add(snack);
			w->Add(lb2);
			w->Add(opt);	
			w->Add(opt2);
			w->Add(opt3);
			w->Add(opt4);
			w->Add(opt5);
			w->Add(tot);
			w->Add(gb);
		});
		
		//Creating list of genres
		std::vector<std::string> genres;
		genres.push_back("Action");
		genres.push_back("Comedy");
	        genres.push_back("Horror");
		genres.push_back("Thriller");
		genres.push_back("Zach's Top Picks");
		Label *rec = new Label(30,7,"");
		Label *rec2 = new Label(30,8, "");
		Label *rec3 = new Label(30,9, "");
	        List *lister = new List(5,7,genres);
		lister->AddListener([&](int i){
			string selected = lister->GetSelected();
			
			//Placing text based on selection
			if(selected == "Action"){
				rec->SetText("Mad Max");
				rec2->SetText("Skyfall");
				rec3->SetText("The Mechanic");
			}else if(selected == "Comedy"){
				rec->SetText("Tommy Boy");
				rec2->SetText("Dumb and Dumber");
				rec3->SetText("Talladega Nights: The Ballad of Ricky Bobby");
			}else if(selected == "Horror"){
				rec->SetText("Hush");
				rec2->SetText("IT");
				rec3->SetText("Get Out");
			}else if(selected == "Thriller"){
				rec->SetText("Shutter Island");
				rec2->SetText("Memento");
				rec3->SetText("Django Unchained");
			}else{
				rec->SetText("Inglourious Basterds");
				rec2->SetText("A Quiet Place");
				rec3->SetText("No Country For Old Men");
			}
		});

		//Adding to widget
		w->Add(rec);
		w->Add(rec2);
		w->Add(rec3);
		w->Add(lb);	
	        w->Add(title);
		w->Add(desc);
	        w->Add(lister);
		w->Add(foodOpt);
	        w->Draw();
	        w->Loop();

        return 0;
}
