#include <functional>
#include <string>
#include <ncurses.h>
#include "Widget.h"
#include <list>
#include <iterator>
#pragma once

using namespace std;

class Button : public Widget {
	private:
		string buttonLabel;
		std::list<std::function<void()>> listeners;
	public:
		Button(int, int, const string);
		void Draw() override;
		void AddListener(std::function<void()>);
		void OnMouseClick(int, int) override;
};
