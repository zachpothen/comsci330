#include "Button.h"

	Button::Button(int x, int y, const string s)
		:Widget(x,s.length()-1+x,y,y){
		buttonLabel = s;
		rect->SetLeft(x);
	}
	
	void Button::Draw(){
		attron(A_REVERSE);
		mvprintw(GetTop(), GetLeft(), buttonLabel.data());
		attroff(A_REVERSE);
	}

	void Button::AddListener(std::function<void()> listener){
		this->listeners.push_front(listener);
	}

	void Button:: OnMouseClick(int x, int y){
		std::list<std::function<void()>>::iterator iter = listeners.begin();	
		while (iter != listeners.end()){
			(*iter)();
			iter++;
		}
	}
