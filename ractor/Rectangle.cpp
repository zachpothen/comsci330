#include "Rectangle.h"

	Rectangle::Rectangle(int l, int r, int t, int b) {
		top = t;
		bottom = b;
		left = l;
		right = r;
	}
	
	int Rectangle::GetLeft() const{
		return left;
	}
	
	int Rectangle::GetRight() const{
		return right;
	}
	
	int Rectangle::GetTop() const{
		return top;
	}
	
	int Rectangle::GetBottom() const{
		return bottom;
	}

	void Rectangle::SetLeft(int l){
		left = l;
	}

	void Rectangle::SetRight(int r){
		right = r;
	}

	void Rectangle::SetTop(int t){
		top = t;
	}

	void Rectangle::SetBottom(int b){
		bottom = b;
	}
	
	int Rectangle::GetWidth() const{
		return right - left + 1;
	}

	int Rectangle::GetHeight() const{
		return bottom - top + 1;
	}
	
	bool Rectangle::Contains(int x, int y) const{
		if(x >= left && x <= right && y >= top && y <= bottom) {
			return true;
		}else{
			return false;
		}
	}

	bool Rectangle::Intersects(const Rectangle r){
		if(Contains(r.GetRight(), r.GetBottom())||Contains(r.GetRight(),r.GetBottom())||Contains(r.GetLeft(),r.GetTop())||Contains(r.GetLeft(),r.GetBottom())){
		return true;
	}else{
		return false;
	
	}
	}
