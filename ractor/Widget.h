#include "Rectangle.h"
#include <ncurses.h>
#pragma once

using namespace std;

class Widget{
	protected:
	Rectangle *rect;	
	public:
	Widget(int, int, int, int);
	virtual ~Widget();
	bool Contains(int, int) const;
	int GetLeft() const;
	int GetRight() const;
	int GetTop() const;
	int GetBottom() const;
	virtual void Draw() = 0;
	virtual void OnMouseClick(int, int);
};
