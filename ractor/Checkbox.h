#include "Widget.h"
#include <string>
#include <functional>
#include <iterator>
#include <list>
//avoid redefinitions
#pragma once
#include <ncurses.h>
using namespace std;

class Checkbox : public Widget{
	private:
		std::list<std::function<void(bool)>> listeners;
		bool checked;
		string CheckboxLabel;
	public:
		Checkbox(int, int, const string);
		
		bool IsChecked() const;
		void IsChecked(bool);
		void Draw(void) override;
		void AddListener(std::function<void(bool)>);
		void OnMouseClick(int, int) override;
};
