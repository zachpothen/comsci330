#include <string>
#include <ncurses.h>
#include "Widget.h"

using namespace std;

class Label : public Widget {
	private:
		string text;
	public:	
		Label(int, int, const string);
		void Draw() override;
		void SetText(const string&);
};
