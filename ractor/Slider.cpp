#include "Slider.h"

        Slider::Slider(int x, int y, int m, int d)
		:Widget(x,x+m,y,y){
                this->x = x;
                this->y = y;
                max = m;
		def = d;
        }
	
	Slider::~Slider(){

	}
	
	int Slider::GetValue() const{
		return def;
	}

	int Slider::GetMax() const{
		return max;
	}

        void Slider::Draw(){

		char arr[max+1];
		for(int i = 0; i <= max; i++){
			//fill the array with . if its at the value add o
			if(i == def){
				arr[i] = 'o';
			}else{
				arr[i] = '.';
			}
		}
		arr[max+1] = 0;
		mvprintw(GetTop(), GetLeft(), arr);		
        }

        void Slider::AddListener(std::function<void(int)> listener){
		this->listeners.push_front(listener);
        }

        void Slider::OnMouseClick(int x, int y){
		std::list<std::function<void(int)>>::iterator iter = listeners.begin();
		while(iter != listeners.end()){
			(*iter)(x);
			iter++;
		}
		def = x;
		Draw();
        }
