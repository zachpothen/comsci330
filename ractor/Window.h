#include <string>
#include <list>
#include <functional>
#include <iterator>
#include "Widget.h"
#include <vector>

#pragma once

using namespace std;

class Window : public Widget {
	private:
		std::list<Widget*> list;
		bool cont;
	public:
		Window();
		~Window();
		void Add(Widget*);
		void Draw(void) override;
		void Loop(void);
		void Stop();
};
