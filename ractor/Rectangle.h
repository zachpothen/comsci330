#include <string>
#include <ncurses.h>
#pragma once

class Rectangle{
	private:
	int top, bottom, left, right;
	public:
	Rectangle(int, int, int, int);
	int GetLeft() const;
	int GetRight() const;
	int GetTop() const;
	int GetBottom() const;
	void SetLeft(int);
	void SetRight(int);
	void SetTop(int);
	void SetBottom(int);
	int GetWidth() const;
	int GetHeight() const;
	bool Contains(int, int) const;
	bool Intersects(const Rectangle);
};
