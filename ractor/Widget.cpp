#include "Widget.h"

	Widget::Widget(int l, int r, int t, int b){
		rect = new Rectangle(l,r,t,b);
	}
	
	Widget::~Widget(){
	}

	int Widget::GetLeft() const{
		return rect->GetLeft();
	}

	int Widget::GetRight() const{
		return rect->GetRight();
	}
	
	int Widget::GetTop() const{
		return rect->GetTop();
	}

	int Widget::GetBottom() const{
		return rect->GetBottom();
	}

        bool Widget::Contains(int x, int y) const{
                if(x >= rect->GetLeft() && x <= rect->GetRight() && y >= rect->GetTop() && y <= rect->GetBottom()) {
                        return true;
                }else{
                        return false;
                }
        }

	void Widget::OnMouseClick(int x, int y){
	}

