#include <string>
#include <list>
#include <functional>
#include <iterator>
#include "Widget.h"
#include <vector>
#pragma once
#include <ncurses.h>
using namespace std;
class List : public Widget{
        private:
                bool atIndex;
		int index;
		std::list<std::function<void(int)>> list;
		std::vector<std::string> listItem;
	public:
                List(int, int, const std::vector<std::string>);

                int GetSelectedIndex() const;
                string& GetSelected() const;
                void Draw(void) override;
                void AddListener(std::function<void(int)>);
                void OnMouseClick(int, int);
};                                   
