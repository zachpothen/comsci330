#include "List.h"

        List::List(int x, int y, const std::vector<std::string> s)
		:Widget(x, x, y, y){
		atIndex = false;
		int max = 0, nItems = 0;

		for(std::string lString : s){
			//Put String into the list to display on screen
			listItem.push_back(lString);
				
			//Find the longest lStringing to set max size
			if(lString.length() > max){
				max = lString.length();
			}
				//increment the amount of string in the list
				nItems++;
		}
		
		//set the right and botom using max and nItems	
		if(nItems > 0){
			rect->SetRight(x+max-1);
			rect->SetBottom(y+nItems-1);
		}
		//set none selected right away	
		index = -1;
        }

	int List::GetSelectedIndex() const{
		return index;
	}
        
	string& List::GetSelected() const{
		return (string&)listItem.at(index);
		
	}
	
	void List::Draw(){
		int nItems = 0;
		for(std::string lString : listItem){
			if(nItems == index){
				attron(A_REVERSE);
				mvprintw(GetTop()+nItems, GetLeft(), lString.data());
				attroff(A_REVERSE);
			}else{
				mvprintw(GetTop()+nItems, GetLeft(), lString.data());
			}
		nItems++;

		}	
        }

        void List::AddListener(std::function<void(int)> listener){
		this->list.push_front(listener);
        }

        void List::OnMouseClick(int x,int y){
			if(y == index){
				index = -1;
			}else{
				index = y;
			}
		std::list<std::function<void(int)>>::iterator iter = list.begin();
		while(iter != list.end()){
			(*iter)(index);
			iter++;
		}
	Draw();
}
