#include <string>
#include <list>
#include <functional>
#include <iterator>
#include <vector>
#include "Widget.h"
#pragma once
#include <ncurses.h>
using namespace std;

class Slider : public Widget {
	
	private:
		int x, y, max, def;
		std::list<std::function<void(int)>> listeners;
	public:
		Slider(int, int, int, int);
		~Slider();
		int GetValue() const;
		int GetMax() const;
		void Draw(void) override;
		void AddListener(std::function<void(int)>);
		void OnMouseClick(int, int) override;
};
