#include "Window.h"
#include <ncurses.h>

	Window::Window()
		:Widget(-1,-1,-1,-1){
		initscr();
		noecho();
		curs_set(FALSE);
		keypad(stdscr, TRUE);
		mousemask(BUTTON1_CLICKED, NULL);
	}
	
	Window::~Window(){
		for(Widget *w : list){
			delete w;
		}
		list.clear();
	}

	void Window::Add(Widget *w){
		list.push_front(w); 
	}

	void Window::Draw(void){
		for(Widget* w : list){
			w->Draw();
		}
	}

	void Window::Loop(){
		cont = true;
		MEVENT event;
		while(cont){
			clear();
			Draw();		
			int input = getch();
			if(input == 'q'){
				Stop();
			}else if(input == KEY_MOUSE){
			
				if(getmouse(&event) == OK){
					for(Widget *w : list){
						if(w->Contains(event.x, event.y)){
							w->OnMouseClick(event.x - w->GetLeft(), event.y - w->GetTop());
						}
					}
				}
				refresh();
			}
		}
		endwin();
	}

	void Window::Stop(){
		cont = false;
	}
