#include "Label.h"

	Label::Label(int x, int y, const string s)
		:Widget(x,s.length()-1 + x, y, y){
		text = s;
	}
	
	void Label::Draw(){
		mvprintw(GetTop(), GetLeft(), text.data());
	}

	void Label::SetText(const string& s)
	{
		text = s;
		//adjust the size of the right hand side
		rect->SetRight(text.length()-1+rect->GetLeft());
	}
